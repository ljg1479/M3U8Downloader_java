package com.lansosdk.util;

import android.content.Context;

import com.lansosdk.videoeditor.VideoEditor;

import java.io.File;
import java.util.List;

/**
 * 注意: 此代码仅作为视频处理的演示使用, 不属于sdk的一部分.
 */
public class FFmpegFunctions {

    public final static String TAG = "FFmpegFunctions";


    /**
     * 两个音频在混合时,第二个延时一定时间.
     */
    public static String demoAudioDelayMix(Context ctx, VideoEditor editor, String mainAudio, String assetPath) {
        String secondAudio = CopyFileFromAssets.copyAssets(ctx, assetPath);
        return editor.executeAudioDelayMix(ctx,mainAudio, secondAudio, 3000, 3000);
    }

    /**
     * 两个音频在混合时调整音量
     */
    public static String demoAudioVolumeMix(Context ctx,VideoEditor editor, String mainAudio, String bgmAudio) {
        return editor.executeAudioVolumeMix(ctx,mainAudio, bgmAudio, 5f, 0.5f);
    }

    /**
     * 两个音轨map到mp4容器中
     */
    public static String mixAudioToMP4(Context ctx,VideoEditor editor, String mainAudio, String audioStr2) {
        return editor.executeMixAudioToMP4(ctx,mainAudio, audioStr2);
    }

    /**
     * 视频转码 m3u8转MP4
     * @param ctx
     * @param editor
     * @param m3u8Path
     * @param mp4Path
     * @return
     */
    public static String executeM3U8ToMP4(Context ctx,VideoEditor editor,String m3u8Path, String mp4Path) {
        return editor.executeM3U8ToMP4(ctx,m3u8Path, mp4Path);
    }

    /**
     * 一个音轨map到mp4容器中
     */
    public static String executeMP3ToMP4(Context ctx, VideoEditor editor, String mainAudio) {
        return editor.executeMP3ToMP4(ctx,mainAudio);
    }


    public static String executeAMRToMP3(Context ctx,VideoEditor editor, String mainAudio){
        return editor.executeAMRToMP3(ctx,mainAudio);
    }

//    public static String executeContactArm(Context ctx, VideoEditor editor, List<File> list){
//        return editor.contactArm(ctx,list);
//    }
    public static String executeContactArm(Context ctx, VideoEditor editor, List<String> list){
        return editor.contactArm(ctx,list);
    }

}
